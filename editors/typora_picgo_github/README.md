# Typora + Picgo + GitHub on Ubuntu



[TOC]

---

## Reference

Blog: https://blog.csdn.net/phfred/article/details/121315344

Typora: https://typora.io/

PicGo: https://github.com/Molunerfinn/PicGo

* PicGo-xxx.AppImage (for Ubuntu)



---

## Configure picgo for Typora based on GitHub



### Install nodejs, npm

```cmd
sudo apt-get install nodejs npm -y
```



### Install picgo command

```cmd
sudo npm install -g picgo
```



### Precondition in GitHub

1. The repository for storing Pictures: https://github.com/liushuimpc/ImageHost
2. Get your personal token from GitHub.



### Configure picgo uploader

```cmd
$ picgo set uploader
? Choose a(n) uploader github
设定仓库名 格式：username/repo liushuimpc/ImageHost
设定分支名 例如：main main
? token: [hidden]
设定存储路径 例如：test/ test/
设定自定义域名 例如：https://test.com https://raw.githubusercontent.com/liushuimpc/ImageHost/main
[PicGo SUCCESS]: Configure config successfully!
```



### Configure Typora Preference

```mermaid 
graph LR

A[Typora] --> B[File] --> C[Preference] --> D[Image] --> E[Image Upload Setting]

```

![typora-image-preference](https://raw.githubusercontent.com/liushuimpc/ImageHost/main/test/typora-preferences.png)

1. select "Custom Command"
2. Input command: "/usr/local/bin/picgo upload"



-----

## Done

Now you are able to use <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd> to insert Picture, and upload the Picture to your GitHub automatically!

