# Sample

[TOC]



## chapter

- how to test
- what's the expectation?
  - t1
  - t2



## Steps

1. go
2. enter
3. out
   - a
   - b
     - c
       - d
   - f

| Title1 | Title2 | Title3        |
| :----- | ------ | :------------ |
| aaa    | bbbbbb | ccccccccccccc |
| how    | when   | what          |
|        |        |               |

## Lines

- **Bold text**
- ~~remove~~
- <u>underline</u>



---

<xx> link

[name] (address)

<www.baidu.com>

[www.google.com](www.google.com)







## Flowchar

```mermaid
graph LR
A[Hard edge] --> B(Round edge)
B-->C{Decision}
C-->|One| D{Result 1}
C-->|Two| E{Result 2}
```



```mermaid
graph LR
A[Step1] --> B[Step2]
C[Step3] --> D[Step4]
B --> E[Step5]
D --> E[Step5]
```



## Block

> what's this?
>
> This is a block
>
> start by >
>
> > what's this?
> >
> > This is internal block.
> >
> > 1. one
> > 2. two
> >
> > * a
> > * b
>
> End



1. step 1

   > what?
   >
   > internal block inside chapter

2. step 2



## Code

```java
System.out.println("what's this?");
return 0;
```



```c
int main(void)
{
    int a, b = 1;
    b = a * 2;
    printf("b=" + b);
    return 0;
}
```





## Hightlight

`highlight`

`line 1`

`line 2`



==what==

==how==









## Picture

![Pic]  (/home/marco/Pictures/wallpaper/Jisoo-a.jpg)

![Pic](/home/marco/Pictures/wallpaper/Jisoo-a.jpg)



## PicGo test

![test](/home/marco/Pictures/wallpaper/main-qimg-a4d37b880fcc360982b15dd89ba9809d.jpeg)





## html

Use <kbd></kbd> + <kbd></kbd> + <kbd></kbd> to reboot your computer.

Use <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Del</kbd> to reboot your computer.



H~2~O

X^2^





:happy:

:cry:

![pic2](https://raw.githubusercontent.com/liushuimpc/ImageHost/main/test/Jisoo-a.jpg)



## Gantt

```mermaid
gantt
dateFormat YYYY-MM-DD
title Adding GANTT diagram functionality to mermaid
section 现有任务
已完成 :done, des1, 2014-01-06,2014-01-08
进行中 :active, des2, 2014-01-09, 3d
计划一 : des3, after des2, 5d
计划二 : des4, after des3, 5d
```


## UML

```mermaid
sequenceDiagram
张三 ->> 李四: 你好！李四, 最近怎么样?
李四 ->>王五: 你最近怎么样，王五？
李四 -x 张三: 我很好，谢谢!
李四 -x 王五: 我很好，谢谢!
Note right of 王五: 李四想了很长时间, 文字太长了不适合放在一行??

李四 ->>张三: 打量着王五…
张三 ->>王五: 很好… 王五, 你怎么样?
```


## Flowchart

````flow
st=>start: 开始
e=>end: 结束
op=>operation: 我的操作
cond=>condition: 确认？
st->op->cond
cond(yes)->e
cond(no)->op
````

