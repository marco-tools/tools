import os
import pandas as pd

# Change these paths to your source and destination folders
source_folder = '/path-of-source-folder'  # Folder containing .csv files
destination_folder = '/path-of-dest-folder'  # Folder to save .xlsx files

# Ensure the destination folder exists
os.makedirs(destination_folder, exist_ok=True)

# Loop through all files in the source folder
for filename in os.listdir(source_folder):
    if filename.endswith('.csv'):  # Check if the file is a .csv
        # Define the full path to the .csv file
        csv_file_path = os.path.join(source_folder, filename)
        
        # Define the new filename with a .xlsx extension
        excel_file_path = os.path.join(destination_folder, filename.replace('.csv', '.xlsx'))
        
        # Read the .csv file and convert it to .xlsx
        try:
            df = pd.read_csv(csv_file_path)  # Read the .csv file
            df.to_excel(excel_file_path, index=False)  # Save as .xlsx
            print(f'Successfully converted: {filename} to {excel_file_path}')
        except Exception as e:
            print(f'Failed to convert {filename}: {e}')

print("Conversion complete.")
