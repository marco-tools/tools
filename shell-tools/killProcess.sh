#!/usr/bin/bash
#
# Use "ps" to get the process, and kill it.
# It's useful when there are a lot of processes in an application
#
# Example:
# ./killProcess.sh teams
# ./killProcess.sh chrome
#
# TODO
# For this scenario, it could be improved by executing `ps -e`

output_first_line=`ps -ef | grep $1 | head -1`
#echo $output_first_line

output=${output_first_line#* }
#echo output1=$output

# method1
output_no_space1=`echo $output | xargs`
#echo output_no_space1=$output_no_space1

# method2
output_no_space2=`echo "${output#"${output%%[![:space:]]*}"}"`
#echo output_no_space2=$output_no_space2

pid=${output_no_space1%% *}
echo pid=$pid

kill $pid
